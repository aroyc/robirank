/*
 * Copyright (c) 2014 Hyokun Yun, Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#include "loadlibsvm.hpp"
#include <cassert>

#undef __FUNCT__
#define __FUNCT__ "parse_file"
PetscErrorCode parse_file(FILE* input, DataCtx* data)
{  
  // The job of this function is to calculate:
  // 1. number of lines (data->m)
  // 2. number of nnz for all lines (data->nnz_array)
  // 3. number of features (data->maxnnz)
  // 4. check if indexing starts with 1
  // 5. build the qid to idx map
  
  PetscErrorCode info;
  size_t   len=0;  // buffer length
  PetscInt llen=0; // number of characters in a line
  char*    line=0;
  PetscInt m_guess=128;
  
  PetscFunctionBegin;
  data->maxlen=0;
  data->nnz_array=0;
  data->one_offset=PETSC_TRUE;
  data->qid_idx_map.clear();

  // Guess the number of training examples
  info=PetscMalloc(m_guess*sizeof(PetscInt),&data->nnz_array); CHKERRQ(info);

  data->dim=-1;
  data->m=0;
  data->maxnnz=0;

  PetscInt qid=0;

  // parse input file line by line
  while ((llen=getline(&line, &len, input)) != -1){
    if (llen > data->maxlen)
      data->maxlen=llen;
    
    // Read and ignore the label
    strtok(line, " \t");
    PetscInt nnz=0;
    // Now process the rest of the line
    while (1){  
      // parse id:val pairs of feature
      char* pidx=strtok(NULL, ":");
      char* pval=strtok(NULL, " \t");
      
      if (pidx==NULL or pval==NULL)
        break;
      
      if(strcmp(pidx, "qid") == 0){
        map<string, int>::iterator it2=data->qid_idx_map.find(string(pval));
        if(it2==data->qid_idx_map.end()){
          data->qid_idx_map[string(pval)]=qid;
          qid++;
        }
      }else{
        PetscInt idx=(PetscInt) strtol(pidx, NULL, 10);
        if(!idx)
          data->one_offset=PETSC_FALSE;
        if (idx > data->dim)
          data->dim=idx;
        nnz++;
      }
    }

    if (data->m >= m_guess){
      PetscInt* nnz_array_temp=0;
      info=PetscMalloc(m_guess*2*sizeof(PetscInt),&nnz_array_temp); CHKERRQ(info);
      info=PetscMemcpy(nnz_array_temp,data->nnz_array,m_guess*sizeof(PetscInt)); CHKERRQ(info);
      info=PetscFree(data->nnz_array); CHKERRQ(info);
      data->nnz_array=nnz_array_temp;
      m_guess *= 2;
    }

    // Add one extra dimension if needed 
    if(data->add_bias)
      nnz++;
    
    // Remember maximum number of nonzeros in any row 
    if(nnz > data->maxnnz)
      data->maxnnz=nnz;
    data->nnz_array[data->m]=nnz;

    data->m++;
  }
  
  if(line)
    free(line);

  //data dim = 1 more than max idx
  data->dim++;
  PetscPrintf(PETSC_COMM_SELF, "pre_dim=%D\n", data->dim);
  // adjust for bias by adding one extra dimension if needed 
  if(data->add_bias)
    data->dim++;
  // If indices start with one in the LibSVM file then account for that
  if(data->one_offset)
    data->dim--;
  PetscPrintf(PETSC_COMM_SELF, "post_dim=%D\n", data->dim);

  // adjust to finalize the array of size
  PetscInt* nnz_array_temp;
  info=PetscMalloc(data->m*sizeof(PetscInt),&nnz_array_temp); CHKERRQ(info);
  info=PetscMemcpy(nnz_array_temp,data->nnz_array,data->m*sizeof(PetscInt)); CHKERRQ(info);
  info=PetscFree(data->nnz_array); CHKERRQ(info);
  data->nnz_array=nnz_array_temp;  

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "parse_line"
PetscErrorCode parse_line(char* line, PetscInt* idxs, PetscScalar* vals, PetscScalar* label, PetscInt m, DataCtx* data)
{

  PetscFunctionBegin;
  
  // Given a line, retrieve the label, and the idx:val pair of features
  map<int, vector<int> > &gs_map=*(data->gs_map);  
  // Read the label
  char *plabel=strtok(line," \t");
  *label=strtol(plabel,NULL,10);
  // process the line
  PetscInt nnz=0;
  while (1){
    char* pidx=strtok(NULL, ":");
    char* pval=strtok(NULL, " \t");

    if (pidx==NULL or pval==NULL)
      break;
    
    if(strcmp(pidx, "qid") == 0){
      PetscInt qid=data->qid_idx_map[string(pval)];
      vector<int> v;
      if(gs_map.count(qid))
        v=gs_map[qid];
      v.push_back(m);
      gs_map[qid]=v;
      continue;
    }else{
      vals[nnz]=strtod(pval, NULL);
      PetscInt idx=(PetscInt) strtol(pidx, NULL, 10);
      if(data->one_offset)
        idx--;
      idxs[nnz]=idx;
      nnz++;
    }
  }
  // Add an extra dimension and set it to one
  if(data->add_bias){
    vals[nnz]=1.0;
    idxs[nnz]=data->dim-1;
    nnz++;
  }
  assert(data->nnz_array[m]==nnz);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "assemble_matrix"
PetscErrorCode assemble_matrix(FILE* input, PetscInt begin, PetscInt end, DataCtx* data)
{  
  // For uniprocessor, fill in the values of the data matrix and label vector
  // only take lines from begin to end-1
  PetscErrorCode info;
  size_t len=data->maxlen;
  char* line=(char *) malloc(len*sizeof(char));
  
  PetscInt m=0;
  PetscInt ii=0;
  PetscScalar label=0;
  PetscScalar* vals=0;
  PetscInt* idxs=0;
  map<string, int> qid_xmap;  

  PetscFunctionBegin;

  info=PetscMalloc(data->maxnnz*sizeof(PetscScalar),&vals); CHKERRQ(info);
  info=PetscMalloc(data->maxnnz*sizeof(PetscInt), &idxs); CHKERRQ(info);

  while (getline(&line, &len, input) != -1){
    //skip the lines for which this processor is not responsible
    if(m < begin){
      m++;  
      continue;
    } else if(m >= end) 
      break;
    info=parse_line(line,idxs,vals,&label,m,data); CHKERRQ(info);
    info=VecSetValues(*data->labels,1,&m,&label,INSERT_VALUES); CHKERRQ(info);
    info=MatSetValues(*data->data,1,&m,data->nnz_array[m],idxs,vals,INSERT_VALUES); CHKERRQ(info);
    m++; 
    ii++;
  }
  
  free(line);
  info=PetscFree(vals); CHKERRQ(info);
  info=PetscFree(idxs); CHKERRQ(info);

  info=VecAssemblyBegin(*data->labels); CHKERRQ(info);
  info=VecAssemblyEnd(*data->labels); CHKERRQ(info);

  info=MatAssemblyBegin(*data->data,MAT_FINAL_ASSEMBLY); CHKERRQ(info);
  info=MatAssemblyEnd(*data->data,MAT_FINAL_ASSEMBLY); CHKERRQ(info);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "fill_arrays_uni"
PetscErrorCode fill_arrays_uni(FILE* input, DataCtx* data)
{
  // For uniprocessor, create and fill in the data matrix
  PetscErrorCode info;

  PetscFunctionBegin;
  // Allocate space for the labels
  info=VecCreate(PETSC_COMM_WORLD,data->labels); CHKERRQ(info);
  info=VecSetSizes(*(data->labels),PETSC_DECIDE,data->m); CHKERRQ(info);
  info=VecSetFromOptions(*(data->labels)); CHKERRQ(info);
  info=PetscObjectSetName((PetscObject) *(data->labels), "Labels"); CHKERRQ(info);

  info=MatCreate(PETSC_COMM_WORLD,data->data); CHKERRQ(info);
  info=MatSetSizes(*(data->data),PETSC_DECIDE,PETSC_DECIDE,data->m,data->dim); CHKERRQ(info);
  info=MatSetFromOptions(*(data->data)); CHKERRQ(info);

  info=MatSetType(*(data->data), MATSEQAIJ); CHKERRQ(info);
  info=MatSeqAIJSetPreallocation(*(data->data), 0, data->nnz_array); CHKERRQ(info);

  info=PetscObjectSetName((PetscObject) *(data->data),"Data"); CHKERRQ(info);
  info=assemble_matrix(input, 0, data->m, data); CHKERRQ(info);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "reparse_file"
PetscErrorCode reparse_file(FILE* input,PetscInt* diag_nnz, PetscInt* offdiag_nnz, DataCtx* data)
{
  // For multi-processor, collect statistics (nnz) related to diagonal and off-diagonal
  PetscErrorCode info;
  size_t len=data->maxlen;
  char* line=(char *) malloc(len*sizeof(char));
  
  PetscFunctionBegin;
  
  PetscInt begin, end;
  info=MatGetOwnershipRange(*(data->data),&begin,&end); CHKERRQ(info);
  
  PetscInt cbegin, cend;
  info=MatGetOwnershipRangeColumn(*(data->data),&cbegin,&cend); CHKERRQ(info);
  
  PetscInt m=0;
  PetscInt ii=0;
  
  for (PetscInt i=0; i<end-begin; i++)
    diag_nnz[i]=0;
  
  while (getline(&line, &len, input) != -1){
    //skip the lines for which this processor is not responsible
    if(m < begin){
      m++;  
      continue;
    } else if(m >= end) 
      break;
    
    // Read and ignore the label
    strtok(line," \t");
    // Now process the rest of the line
    while (1){
      char* pidx=strtok(NULL,":");
      char* pval=strtok(NULL," \t");

      if (pidx==NULL or pval==NULL)
        break;
      
      if(strcmp(pidx, "qid") == 0){
        continue;
      }else{
        PetscInt idx=(PetscInt) strtol(pidx,NULL,10);
        if(data->one_offset)
          idx--;
        if (idx>=cbegin && idx<cend)
          diag_nnz[ii]++;
      }
    }

    if(data->add_bias && data->dim>cbegin && data->dim-1<cend)
      diag_nnz[ii]++;
    
    offdiag_nnz[ii]=data->nnz_array[m] - diag_nnz[ii];
    m++;    ii++;
  }
  
  if(line)
    free(line);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "fill_arrays_parallel"
PetscErrorCode fill_arrays_parallel(FILE* input, DataCtx* data)
{
  PetscErrorCode info;

  PetscFunctionBegin;
  // Create labels vector 
  info=VecCreate(PETSC_COMM_WORLD,data->labels); CHKERRQ(info);
  info=VecSetSizes(*(data->labels),PETSC_DECIDE,data->m); CHKERRQ(info);
  info=VecSetFromOptions(*(data->labels)); CHKERRQ(info);
  info=VecSetType(*(data->labels),VECMPI);
  info=PetscObjectSetName((PetscObject) *(data->labels),"Labels"); CHKERRQ(info);

  // Create data matrix 
  info=MatCreate(PETSC_COMM_WORLD,data->data); CHKERRQ(info);
  info=MatSetSizes(*(data->data),PETSC_DECIDE,PETSC_DECIDE,data->m,data->dim); CHKERRQ(info);
  info=MatSetFromOptions(*(data->data)); CHKERRQ(info);
  info=MatSetType(*(data->data),MATMPIAIJ); CHKERRQ(info);    
  info=PetscObjectSetName((PetscObject) *(data->data), "Data"); CHKERRQ(info);

  // Allocate space for the data 
  PetscInt begin, end;
  info=MatGetOwnershipRange(*(data->data),&begin,&end); CHKERRQ(info);

  PetscInt *diag_nnz, *offdiag_nnz;
  info=PetscMalloc((end-begin)*sizeof(PetscInt), &diag_nnz); CHKERRQ(info);
  info=PetscMalloc((end-begin)*sizeof(PetscInt), &offdiag_nnz); CHKERRQ(info);

  reparse_file(input, diag_nnz, offdiag_nnz, data);
  rewind(input);
  info=MatMPIAIJSetPreallocation(*(data->data), 0, diag_nnz, 0, offdiag_nnz); CHKERRQ(info);
  info=PetscFree(diag_nnz); CHKERRQ(info);
  info=PetscFree(offdiag_nnz); CHKERRQ(info);

  assemble_matrix(input, begin, end, data);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "LoadLibsvm"
PetscErrorCode LoadLibsvm(AppCtx *user)
{
  // Load from data files in LIBSVM format
  PetscErrorCode    info;
  PetscBool         flg;
  DataCtx           train; 
  DataCtx           test; 
  FILE              *data_file;
  FILE              *tdata_file;
  
  PetscFunctionBegin;
  // Get the name of the files that store the training and test examples
  // Note for LIBSVM inputs, label and feature are stored in one file
  info=PetscOptionsGetString(PETSC_NULL, "-train", train.data_path, PETSC_MAX_PATH_LEN-1, &flg); CHKERRQ(info);
  if (!flg)
    SETERRQ(PETSC_ERR_SUP, 1, "No training data file specified!");
  info=PetscOptionsGetString(PETSC_NULL, "-test", test.data_path, PETSC_MAX_PATH_LEN-1, &flg); CHKERRQ(info);
  if (!flg)
    SETERRQ(PETSC_ERR_SUP, 1, "No test data file specified!");

  info=PetscOptionsGetBool(PETSC_NULL, "-bias", &train.add_bias, &flg); CHKERRQ(info);
  if(flg){
    test.add_bias=train.add_bias;
  }else{
    test.add_bias=PETSC_FALSE;
    train.add_bias=PETSC_FALSE;
  }
  // First parses the training and test data file to collect statistics
  train.data=&user->data;
  train.labels=&user->labels;
  train.gs_map=&user->gs_map;
  test.data=&user->tdata;
  test.labels=&user->tlabels;
  test.gs_map=&user->tgs_map;

  // Master node parses the training data file to collect statistics
  PetscPrintf(PETSC_COMM_WORLD, "Start parsing training data\n");
  info=PetscFOpen(PETSC_COMM_SELF, train.data_path, "r", &data_file); CHKERRQ(info); 
  train.nnz_array=NULL;
  if (user->rank==0) 
  {    
    parse_file(data_file, &train);    
    rewind(data_file);   // Set file pointer to beginning of file
  }
  // Master node parses the test data file to collect statistics
  PetscPrintf(PETSC_COMM_WORLD, "Start parsing test data\n");
  test.nnz_array=NULL;
  info=PetscFOpen(PETSC_COMM_SELF, test.data_path, "r", &tdata_file); CHKERRQ(info);
  if (user->rank==0)   
  {   
    parse_file(tdata_file, &test);    
    rewind(tdata_file);   // Set file pointer to beginning of file
    
    // Set the final dimension to be the larger of the two dimensions    
    test.dim=train.dim=max(train.dim, test.dim);
  }

  // Propagate the statistics of the dataset
  MPI_Bcast(&test.dim,1,MPIU_INT,0,PETSC_COMM_WORLD);  
  MPI_Bcast(&test.m,1,MPIU_INT,0,PETSC_COMM_WORLD);
  MPI_Bcast(&test.maxnnz,1,MPIU_INT,0,PETSC_COMM_WORLD);    // slave nodes need it to load data
  MPI_Bcast(&test.maxlen,1,MPIU_INT,0,PETSC_COMM_WORLD);    // slave nodes need it to load data
  if (user->rank!=0){
    info=PetscMalloc(test.m*sizeof(PetscInt),&test.nnz_array); CHKERRQ(info);
  }
  MPI_Bcast(test.nnz_array,test.m,MPIU_INT,0,PETSC_COMM_WORLD);

  MPI_Bcast(&train.dim,1,MPIU_INT,0,PETSC_COMM_WORLD);  
  MPI_Bcast(&train.m,1,MPIU_INT,0,PETSC_COMM_WORLD);
  MPI_Bcast(&train.maxnnz,1,MPIU_INT,0,PETSC_COMM_WORLD);    // slave nodes need it to load data
  MPI_Bcast(&train.maxlen,1,MPIU_INT,0,PETSC_COMM_WORLD);    // slave nodes need it to load data
  if (user->rank!=0){
    info=PetscMalloc(train.m*sizeof(PetscInt),&train.nnz_array); CHKERRQ(info);
  }
  MPI_Bcast(train.nnz_array,train.m,MPIU_INT,0,PETSC_COMM_WORLD);

  // Really start to load the training data
  PetscPrintf(PETSC_COMM_WORLD, "Start loading the training data\n");
  if (user->size==1)   // uniprocessor
    fill_arrays_uni(data_file, &train);
  else                      // multiprocessor
    fill_arrays_parallel(data_file, &train);

  info=PetscFree(train.nnz_array); CHKERRQ(info);
  PetscFClose(PETSC_COMM_SELF, data_file); 

  // Really load the test data
  PetscPrintf(PETSC_COMM_WORLD, "Start loading the test data\n");
  if (user->size==1)   // uniprocessor
    fill_arrays_uni(tdata_file, &test);
  else                      // multiprocessor
    fill_arrays_parallel(tdata_file, &test);
  info=PetscFree(test.nnz_array); CHKERRQ(info);
  PetscFClose(PETSC_COMM_SELF,tdata_file); 

  info=MatGetSize(user->data, &user->m, &user->dim); CHKERRQ(info);
  info=MatGetSize(user->tdata, &user->tm, &user->tdim); CHKERRQ(info);
  PetscFunctionReturn(0);
}
