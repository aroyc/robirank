/*
 * Copyright (c) 2014 Hyokun Yun, Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#include "tao.h"
#include "appctx.hpp"
#include "loadlibsvm.hpp"
#include "rankloss.hpp"

#include <cmath>
#include <cassert>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

enum {LBFGS=0, BMRM=1};

enum {ROBIRANK=0, INFINITEPUSH=1, LOGISTIC=2, IDENTITY=3, TLOGISTIC=4};

static  char help[]="soft hinge loss and its generalizations using TAO \n \
Input parameters are:\n                                                 \
  -data    <filename> : file which contains data in LibSVM format\n     \
  -labels  <filename> : file which contains labels in LibSVM format\n   \
  -tdata   <filename> : file which contains test data in LibSVM format\n \
  -tlabels <filename> : file which contains test labels in LibSVM format \n \
  -w_file  <string>   : file to load the intial weight vector from \n   \
  -lambda <double>    : regularization parameter \n                     \
  -loss    <int>      : 0=ROBIRANK 1=INFINITEPUSH 2=LOGISTIC 3=IDENTITY 4=TLOGISTIC \n \
  -solver  <int>      : 0=LBFGS 1=BMRM \n \
  -tol <double>       : convergence tolerance \n\n";


// Function pointers to loss and gradient computation functions 
PetscErrorCode (*loss_grad)(PetscScalar *fx_array, 
                            PetscScalar *labels_array, 
                            PetscScalar *grad_array, 
                            PetscInt    n, 
                            PetscScalar *loss,
                            AppCtx      *user);    

// Initialize and finalize the loss 
PetscErrorCode (*init)(AppCtx *user);

PetscErrorCode (*finalize)(void);

// Forwarding function
PetscErrorCode ScalarLossGradBForce(TaoSolver tao, Vec w, double *f, Vec G, void *ctx);

// Predict labels on test data
PetscErrorCode Predict(Vec w, AppCtx* user);

// Check Generalization of intermediate solutions 
PetscErrorCode PrintFunVal(TaoSolver tao, void *ctx);


// Parse the arguments in the command line
PetscErrorCode LoadOptions(AppCtx *user);

// Print a summary of the settings
PetscErrorCode PrintSetting(AppCtx *user);

// Load w vector from file
PetscErrorCode LoadWFromFile(Vec *w, char w_file[], AppCtx *user);

// Allocate the space for work
PetscErrorCode AllocateWorkSpace(Vec *w, AppCtx *user);

// Destroy the work space
PetscErrorCode DestroyWorkSpace(Vec *w, AppCtx *user);


#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{
  AppCtx            user;                  
  PetscErrorCode    info;              
  Vec               w;                     // solution vector
  TaoSolver         tao;                   
  TaoSolverTerminationReason reason;
  
  // Initialize TAO and PETSc
  info = PetscInitialize(&argc, &argv, (char *)0, help);
  info = TaoInitialize(&argc, &argv, (char *)0, help);
  
  MPI_Comm_rank(PETSC_COMM_WORLD, &user.rank);
  MPI_Comm_size(PETSC_COMM_WORLD, &user.size);

  // Check for command line arguments to override defaults
  info = LoadOptions(&user);
  info =   LoadLibsvm(&user);
  info = AllocateWorkSpace(&w, &user);   // Allocate space 
  
  if (user.rank==0) {
    init(&user);
    PrintSetting(&user);
  }

  // Start the timer 
  if (user.rank==0)
    user.opt_timer.start();  

  ///////////////////////////////////////////////////////////////////
  // TAO code begins here
  info = PetscPrintf(PETSC_COMM_WORLD, "tol=%e\n", user.tol); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_WORLD, "lambda=%lf\n", user.lambda); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_WORLD, "alpha=%e\n", user.alpha); CHKERRQ(info);
  if(user.w_file[0] != '\0')
    info = LoadWFromFile(&w, user.w_file, &user);
    
  user.num_fn_eval = 0;

  // Create TAO solver with desired solution method 
  info = TaoCreate(PETSC_COMM_WORLD, &tao); CHKERRQ(info);
  if(user.solver==LBFGS){
    info = TaoSetType(tao, "tao_lmvm"); CHKERRQ(info);
  } else {
    info = TaoSetType(tao, "tao_bmrm"); CHKERRQ(info);
    char lambda_buf[20];
    sprintf(lambda_buf,"%lf",user.lambda);
    PetscOptionsSetValue("-tao_bmrm_lambda",lambda_buf);
  }
  
  info = TaoSetInitialVector(tao, w); CHKERRQ(info);
  info = TaoSetObjectiveAndGradientRoutine(tao, ScalarLossGradBForce, &user); CHKERRQ(info);
  info = TaoSetTolerances(tao, user.tol, 0, user.tol, 0, 0); CHKERRQ(info);
  info = TaoSetMaximumIterations(tao, 50000); CHKERRQ(info);
  info = TaoSetMaximumFunctionEvaluations(tao, 15000); CHKERRQ(info);
    
    
  // Check for TAO command line options 
  info = TaoSetFromOptions(tao); CHKERRQ(info);
  
  // Print intermediate function values 
  info = TaoSetMonitor(tao, PrintFunVal, &user, PETSC_NULL); CHKERRQ(info);
    
  // Solve the Application   
  info = TaoSolve(tao); CHKERRQ(info);
    
  info = TaoGetTerminationReason(tao, &reason); CHKERRQ(info);
  PetscPrintf(PETSC_COMM_WORLD, "Tao Termination Reason: %d\n", reason);
    
  // Free TAO data structures 
  info = TaoDestroy(&tao); CHKERRQ(info);
    
  // Output final prediction
  Predict(w, &user);
    
  // Stop the timer
  if (user.rank==0)
    user.opt_timer.stop();
  
  // Print statistics 
  if (user.rank==0){
    PetscPrintf(PETSC_COMM_WORLD, "\nTotal CPU time: %g seconds \n", user.opt_timer.cpu_total);
    PetscPrintf(PETSC_COMM_WORLD, "Total wallclock time: %g seconds \n", user.opt_timer.wallclock_total);
    PetscPrintf(PETSC_COMM_WORLD, "Obj grad CPU time: %g seconds \n", user.objgrad_timer.cpu_total);
    PetscPrintf(PETSC_COMM_WORLD, "Obj grad wallclock time: %g seconds \n", user.objgrad_timer.wallclock_total);
  }

  // Print solution
  //PetscPrintf(PETSC_COMM_WORLD, "\n\nSolution: \n");
  //VecView(w, PETSC_VIEWER_STDOUT_WORLD);  

  // Free some space
  DestroyWorkSpace(&w, &user);
  
  if (user.rank==0)
    finalize();
  
  // Finalize TAO and PETSc
  info = TaoFinalize(); CHKERRQ(info);
  info = PetscFinalize(); CHKERRQ(info);
  
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "LoadOptions"
PetscErrorCode LoadOptions(AppCtx *user)
{
  PetscBool         flg;
  PetscErrorCode    info;

  user->lambda = 1e-1;
  user->alpha = 1;
  user->tol=1e-9;
  user->trunc = 1;
  user->w_file[0] = '\0';
  user->loss = ROBIRANK;
  user->solver = LBFGS;
  
  info = PetscOptionsGetReal(PETSC_NULL, "-lambda", &user->lambda, &flg); CHKERRQ(info);
  info = PetscOptionsGetReal(PETSC_NULL, "-alpha", &user->alpha, &flg); CHKERRQ(info);
  info = PetscOptionsGetReal(PETSC_NULL, "-tol", &user->tol, &flg); CHKERRQ(info);
  info = PetscOptionsGetInt(PETSC_NULL, "-trunc", &user->trunc, &flg); CHKERRQ(info);
  info = PetscOptionsGetString(PETSC_NULL, "-w_file", user->w_file, PETSC_MAX_PATH_LEN-1, &flg); CHKERRQ(info);
  info = PetscOptionsGetInt(PETSC_NULL, "-loss", &user->loss, &flg); CHKERRQ(info);  
  info = PetscOptionsGetInt(PETSC_NULL, "-solver", &user->solver, &flg); CHKERRQ(info);  

  init = rankloss::init; 
  finalize = rankloss::finalize; 
  
  if(user->loss == ROBIRANK){
    loss_grad = rankloss::robirank; 
  } else {
    if(user->loss == INFINITEPUSH)
      loss_grad = rankloss::infinitepush; 
    else if(user->loss == LOGISTIC)
      loss_grad = rankloss::logistic;
    else if(user->loss == IDENTITY)
      loss_grad = rankloss::identity;
    else
      loss_grad = rankloss::tlogistic; 
  }
  
  if(user->solver==LBFGS and user->loss==INFINITEPUSH)
    info = PetscPrintf(PETSC_COMM_SELF, "WARNING WARNING WARNING!!!! Using LBFGS with non-smooth loss. Results are not guaranteed.\n" ); CHKERRQ(info);

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "LoadWFromFile"
PetscErrorCode LoadWFromFile(Vec *w, char w_file[], AppCtx *user)
{
  PetscErrorCode info;
  string line;
  ifstream myfile(w_file);
  PetscInt count=0;
  if (myfile.is_open())
  {
    while(getline(myfile,line))
    {
      if(!line.compare(0, 1, "#")==0)
      {
        double wi=strtod(line.c_str(), NULL);
        VecSetValues(*w, 1, &count, &wi, INSERT_VALUES);
        ++count;
      }
    }
    myfile.close();
  }
  info = VecAssemblyBegin(*w);CHKERRQ(info);
  info = VecAssemblyEnd(*w);CHKERRQ(info);
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "PrintSetting"
PetscErrorCode PrintSetting(AppCtx *user)
{
  PetscErrorCode info;

  info = PetscPrintf(PETSC_COMM_SELF, "Number of processors: %D\n", user->size); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_SELF, "lambda: %e\n", user->lambda); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_SELF, "m_train: %D\n", user->m); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_SELF, "m_test: %D\n", user->tm); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_SELF, "dim: %D\n", user->dim);
 
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "AllocateWorkSpace"
PetscErrorCode AllocateWorkSpace(Vec *w, AppCtx *user)
{
  PetscErrorCode     info;              

  info = VecCreate(PETSC_COMM_WORLD,w); CHKERRQ(info); 
  info = VecSetSizes(*w, PETSC_DECIDE, user->dim); CHKERRQ(info); 
  info = VecSetFromOptions(*w); CHKERRQ(info); 
  info = PetscObjectSetName((PetscObject) *w, "Solution"); CHKERRQ(info); 
  info = VecSet(*w, 0.0); CHKERRQ(info); 
  info = VecAssemblyBegin(*w); CHKERRQ(info); 
  info = VecAssemblyEnd(*w); CHKERRQ(info); 
  
  info = VecDuplicate(user->labels, &user->fx); CHKERRQ(info); 
  info = VecDuplicate(user->tlabels, &user->tfx); CHKERRQ(info); 
  info = VecDuplicate(user->fx, &user->grad); CHKERRQ(info); 
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "DestroyWorkSpace"
PetscErrorCode DestroyWorkSpace(Vec *w, AppCtx *user)
{
  PetscErrorCode     info;              

  // Free some space

  info = MatDestroy(&user->data); CHKERRQ(info);
  info = VecDestroy(&user->labels); CHKERRQ(info);
  info = MatDestroy(&user->tdata); CHKERRQ(info);
  info = VecDestroy(&user->tlabels); CHKERRQ(info);

  info = VecDestroy(&user->fx); CHKERRQ(info);
  info = VecDestroy(&user->tfx); CHKERRQ(info);
  info = VecDestroy(&user->grad); CHKERRQ(info);

  info = VecDestroy(w); CHKERRQ(info);
  return 0;
}
 

#undef __FUNCT__
#define __FUNCT__ "PrintFunVal"
// PrintFunVal - Print intermediate function values 
 
// Input Parameters:
// tao     - tao solver
// ctx     - user defined application context
 
// Output Parameters:
// None. Display intermediate function values  
PetscErrorCode PrintFunVal(TaoSolver tao, void *ctx){
  AppCtx *user = (AppCtx *) ctx;
  PetscErrorCode info;
  PetscScalar f;
  PetscScalar gnorm;
  PetscInt iter;
  Vec w;   
  
  PetscInt partnnz,nnz,nn;
  PetscReal *wptr;
  
  if (user->rank==0)
    user->opt_timer.stop();
  
  info = TaoGetSolutionVector(tao,&w); CHKERRQ(info);

  info = VecGetArray(w,&wptr);CHKERRQ(info);
  info = VecGetLocalSize(w,&nn);CHKERRQ(info);
  partnnz=0;
  for (int i=0;i<nn;i++)
  {
    if (wptr[i]==0.0)
      partnnz++;
  }
  info = VecRestoreArray(w,&wptr);CHKERRQ(info);
  MPI_Barrier(PETSC_COMM_WORLD);
  MPI_Allreduce(&partnnz,&nnz,1,MPI_INT,MPI_SUM,PETSC_COMM_WORLD);

  info = TaoGetSolutionStatus(tao, &iter, &f, &gnorm, PETSC_NULL, PETSC_NULL, PETSC_NULL);

  PetscPrintf(PETSC_COMM_WORLD, "\nIter: %d, f: %g, gnorm: %g nz: %d\n", iter,f,gnorm,nnz);
  PetscPrintf(PETSC_COMM_WORLD,"num_fn_eval %d CPU: %g Wallclock: %g grad_t: %g matvec_t %g\n",  user->num_fn_eval, user->opt_timer.cpu_total, user->opt_timer.wallclock_total, user->objgrad_timer.cpu_total, user->matvec_timer.cpu_total);

  if (user->rank==0)
    user->opt_timer.start();
   
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "Predict"
// Predict - Predict on test set and evaluate accuracy

// Input Parameters:
// w       - current weight vector
// user    - Application context

// Output Parameters:
// None. Display accuracy 

PetscErrorCode Predict(Vec w, AppCtx* user){
  PetscErrorCode info;
  PetscScalar *tfx_array,*tlabels_array;
  PetscInt low1, high1;
  PetscInt tcorrect=0,total_tcorrect=0;
  PetscScalar overall_avg = 0;
  
  info = MatMult(user->tdata,w,user->tfx); CHKERRQ(info);
  info = VecGetArray(user->tfx,&tfx_array); CHKERRQ(info);
  info = VecGetArray(user->tlabels,&tlabels_array); CHKERRQ(info);
  info = VecGetOwnershipRange(user->tlabels,&low1,&high1); CHKERRQ(info);

  MPI_Barrier(PETSC_COMM_WORLD);
  MPI_Allreduce(&tcorrect,&total_tcorrect,1,MPI_INT,MPI_SUM,PETSC_COMM_WORLD);
 
  for(int truncation=1; truncation <= user->trunc; ++truncation)
  {
    double ndcg_n = rankloss::avg_ndcg(user->tgs_map, tfx_array, tlabels_array, truncation);
    printf("Average NDCG@%d: %f\n", truncation, ndcg_n);
    overall_avg += ndcg_n; 
  }
  PetscPrintf(PETSC_COMM_WORLD, "Overall Average NDCG (w truncation): %f\n", overall_avg/user->trunc);

  //Code below computes NDCG (without any truncation)
  overall_avg = 0.0;
  for(map<PetscInt, vector<PetscInt> >::iterator it=user->tgs_map.begin(); it!=user->tgs_map.end(); ++it){
    PetscInt q = it->first;
    vector<PetscInt> doc_group = it->second;
    overall_avg += rankloss::ndcg(q, doc_group, tfx_array, tlabels_array, doc_group.size());
  } 
  overall_avg = overall_avg/user->tgs_map.size();
  PetscPrintf(PETSC_COMM_WORLD, "Overall NDCG: %f\n", overall_avg);
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "ScalarLossGradBForce"
// ScalarLossGradBForce - Evaluates a scalar loss and its gradient. 

// Input Parameters:
// w       - current weight vector
// ctx     - optional user-defined context

// Output Parameters:
// G - vector containing the newly evaluated gradient
// f - function value

PetscErrorCode ScalarLossGradBForce(TaoSolver tao, 
                                    Vec w, 
                                    double *f, 
                                    Vec G, 
                                    void *ctx){  
  AppCtx *user=(AppCtx *) ctx;  
  PetscErrorCode info;
  PetscScalar reg=0.0;
  PetscScalar *fx_array, *labels_array, *grad_array;
  
  user->num_fn_eval++;

  Predict(w, user);

  if(user->rank == 0){
    user->objgrad_timer.start();
  }

  if(user->rank == 0)
    user->matvec_timer.start();
  
  info = MatMult(user->data,w,user->fx); CHKERRQ(info);
  
  if (user->rank == 0)
    user->matvec_timer.stop();

  // Master node computes the function and gradient coefficients 
  double partloss = 0.0;
  PetscInt low1,high1;
  info=VecGetOwnershipRange(user->labels,&low1,&high1);CHKERRQ(info);

  info = VecGetArray(user->fx,&fx_array); CHKERRQ(info);
  info = VecGetArray(user->labels,&labels_array); CHKERRQ(info);
  info = VecGetArray(user->grad,&grad_array);  CHKERRQ(info);
    
  loss_grad(fx_array,labels_array,grad_array,high1-low1,&partloss,user);
    
  info = VecRestoreArray(user->fx,&fx_array); CHKERRQ(info);
  info = VecRestoreArray(user->labels,&labels_array); CHKERRQ(info);
  info = VecRestoreArray(user->grad, &grad_array);  CHKERRQ(info);
  
  MPI_Barrier(PETSC_COMM_WORLD);
  *f = 0.0;
  MPI_Allreduce(&partloss,f,1,MPI_DOUBLE,MPI_SUM,PETSC_COMM_WORLD);

  
  // Now everybody can compute the gradient
  if (user->rank == 0)
    user->matvec_timer.start();

  info = MatMultTranspose(user->data, user->grad, G); CHKERRQ(info);

  if (user->rank == 0)
    user->matvec_timer.stop();

  // We need to add regularization only for LBFGS
  if(user->solver == LBFGS){
    info = VecDot(w,w,&reg); CHKERRQ(info);
    reg *= user->lambda/2.0;
    *f = *f + reg;
    info = VecAXPY(G, user->lambda, w); CHKERRQ(info);
  }
  
  if (user->rank == 0)
    user->objgrad_timer.stop();
  return 0;
}
